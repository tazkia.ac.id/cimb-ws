package id.ac.tazkia.payment.cimb.command;

import id.ac.tazkia.payment.cimb.dto.VaRequest;
import id.ac.tazkia.payment.cimb.dto.VaRequestStatus;
import id.ac.tazkia.payment.cimb.dto.VaResponse;
import id.ac.tazkia.payment.cimb.dto.VaStatus;
import id.ac.tazkia.payment.cimb.entity.AccountStatus;
import id.ac.tazkia.payment.cimb.entity.VirtualAccount;
import id.ac.tazkia.payment.cimb.exception.VirtualAccountAlreadyPaidException;
import id.ac.tazkia.payment.cimb.exception.VirtualAccountNotFoundException;
import id.ac.tazkia.payment.cimb.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class UpdateVaHandler implements VaHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateVaHandler.class);

    @Value("${cimb.client-id}") private String clientId;

    @Autowired private PaymentService paymentService;

    @Override
    public boolean supports(VaStatus status) {
        return VaStatus.UPDATE.equals(status);
    }

    @Override
    public VaResponse process(VaRequest request)  {
        LOGGER.info("[VA-REQUEST-UPDATE] - Account No : {}, Account Name :{}, Amount : {}",
                request.getAccountNumber(), request.getName(), request.getAmount());

        VaResponse vaResponse = new VaResponse();
        BeanUtils.copyProperties(request, vaResponse);

        try {
            VirtualAccount va = paymentService.findByAccountNumber(request.getAccountNumber());
            if (va == null) {
                throw new VirtualAccountNotFoundException("VA " + request.getAccountNumber() + " not found");
            }
            if(!AccountStatus.ACTIVE.equals(va.getAccountStatus())){
                throw new VirtualAccountAlreadyPaidException("VA " + request.getAccountNumber() + " not active");
            }
            va.setInvoiceNumber(request.getInvoiceNumber());
            va.setPhone(request.getPhone());
            va.setName(request.getName());
            va.setEmail(request.getEmail());
            va.setDescription(request.getDescription());
            va.setAmount(request.getAmount());
            va.setExpireDate(request.getExpireDate());
            paymentService.update(va);
            vaResponse.setRequestStatus(VaRequestStatus.SUCCESS);
            LOGGER.info("[VA-REQUEST-UPDATE] - Success : {}-{}-{}", va.getAccountNumber(), va.getName(), va.getAmount());
        } catch (VirtualAccountAlreadyPaidException | VirtualAccountNotFoundException e) {
            LOGGER.error(e.getMessage(), e);
            vaResponse.setRequestStatus(VaRequestStatus.ERROR);
            LOGGER.error("[VA-REQUEST-UPDATE] - Error : {}", e.getMessage());
        }

        return vaResponse;
    }
}
