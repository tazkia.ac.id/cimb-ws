package id.ac.tazkia.payment.cimb.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

public class RemoveResponseHeaderInterceptor implements EndpointInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(RemoveResponseHeaderInterceptor.class);

    @Override
    public boolean handleRequest(MessageContext messageContext, Object o) throws Exception {
        return true;
    }

    @Override
    public boolean handleResponse(MessageContext messageContext, Object o) throws Exception {
        //LOGGER.debug("Response message : {}", messageContext.getResponse());
        SaajSoapMessage msg = (SaajSoapMessage) messageContext.getResponse();
        msg.getSaajMessage().getSOAPHeader().detachNode();
        return true;
    }

    @Override
    public boolean handleFault(MessageContext messageContext, Object o) throws Exception {
        return true;
    }

    @Override
    public void afterCompletion(MessageContext messageContext, Object o, Exception e) throws Exception {

    }
}
