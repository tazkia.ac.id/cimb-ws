# Integrasi Virtual Account dengan CIMB Niaga

[![pipeline status](https://gitlab.com/tazkia.ac.id/cimb-ws/badges/master/pipeline.svg)](https://gitlab.com/tazkia.ac.id/cimb-ws/commits/master)


## Menjalankan database development ##

```
docker run --rm \
--name cimbws-db \
-e POSTGRES_DB=cimbws \
-e POSTGRES_USER=tazkia \
-e POSTGRES_PASSWORD=tazkia \
-e PGDATA=/var/lib/postgresql/data/pgdata \
-v "$PWD/cimbwsdb-data:/var/lib/postgresql/data" \
-p 5432:5432 \
postgres:13
```